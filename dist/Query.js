"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("@poppinss/utils/build/helpers");
class Query {
    constructor(database, model) {
        this.model = model;
        this.database = database;
    }
    async create(record) {
        let model = new this.model().fill(record);
        await this.database.transaction(async (trx) => {
            model.useTransaction(trx);
            await model.save();
        });
        return model.serialize();
    }
    async update(record) {
        let model = await this.model.findOrFail(record.id);
        await this.database.transaction(async (trx) => {
            model.useTransaction(trx);
            model.merge({ ...record });
            await model.save();
        });
        return model;
    }
    async delete(id) {
        let model = await this.model.findOrFail(id);
        await model.delete();
    }
    async find(query) {
        if (query) {
            const builder = this.model.query();
            this.queryFields(builder, query);
            this.queryWhere(builder, query);
            this.queryWhereHas(builder, query);
            this.queryWhereBetween(builder, query);
            this.querySearch(builder, query);
            this.queryPreloads(builder, query);
            this.queryPreloadsWithWhereHas(builder, query);
            let result = await builder.first();
            return { data: result };
        }
        return { data: null };
    }
    async findOrFail(id) {
        let result = await this.model.findOrFail(id);
        return { data: result };
    }
    async findByOrFail(key, value) {
        let result = await this.model.findByOrFail(key, value);
        return { data: result };
    }
    async search(query) {
        if (query) {
            const builder = this.model.query();
            this.queryFields(builder, query);
            this.queryWhere(builder, query);
            this.queryWhereHas(builder, query);
            this.queryWhereBetween(builder, query);
            this.queryOrderBy(builder, query);
            this.querySearch(builder, query);
            this.queryPreloads(builder, query);
            this.queryPreloadsWithWhereHas(builder, query);
            this.queryWithCounter(builder, query);
            const result = await builder.paginate(query.page || 1, query.limit);
            const serialized = result.serialize();
            const meta = {
                total: serialized.meta.total,
                perPage: serialized.meta.per_page,
                currentPage: serialized.meta.current_page,
                lastPage: serialized.meta.last_page,
                firstPage: serialized.meta.first_page
            };
            return { meta, data: serialized.data };
        }
        return { data: [] };
    }
    queryFields(builder, query) {
        if (query.fields) {
            builder.select(query.fields);
        }
    }
    queryWhere(builder, query) {
        if (query.where) {
            const wheres = Object.entries(query.where);
            for (let index = 0; index < wheres.length; index++) {
                const where = wheres[index];
                if (where[1].v) {
                    let value = where[1].v;
                    let operation = where[1].o;
                    if (operation && operation.includes("LIKE")) {
                        value = operation.replace("LIKE", value).replace(/_/g, "%");
                        operation = "ILIKE";
                    }
                    const fields = where[0].split(':');
                    if (fields.length == 1) {
                        builder.where(where[0], operation || "=", value);
                    }
                    else {
                        builder.where((q) => {
                            for (let index = 0; index < fields.length; index++) {
                                q.orWhere(fields[index], operation || "=", value);
                            }
                        });
                    }
                }
            }
        }
    }
    queryWhereBetween(builder, query) {
        if (query.whereBetween) {
            const whereBetweens = Object.entries(query.whereBetween);
            for (let index = 0; index < whereBetweens.length; index++) {
                const where = whereBetweens[index];
                let value = where[1].v;
                let operation = where[1].o;
                builder.whereBetween(where[0], value);
            }
        }
    }
    queryWhereHas(builder, query) {
        if (query.whereHas) {
            builder.where(subBuilder => {
                this.queryWhereHasRecursive(subBuilder, Object.entries(query.whereHas));
            });
        }
    }
    queryWhereHasRecursive(builder, whereHases) {
        for (let index = 0; index < whereHases.length; index++) {
            const whereHas = whereHases[index];
            if (whereHas[1].v) {
                let value = whereHas[1].v;
                let operation = whereHas[1].o;
                if (operation && operation.includes("LIKE")) {
                    value = operation.replace("LIKE", value).replace(/_/g, "%");
                    operation = "ILIKE";
                }
                const orFields = whereHas[0].split(':');
                if (orFields.length == 1) {
                    builder.where(orFields[0], operation || "=", value);
                }
                else {
                    for (let index = 0; index < orFields.length; index++) {
                        builder.orWhere(orFields[index], operation || "=", value);
                    }
                }
            }
            else {
                builder.whereHas(whereHas[0], (subQuery) => this.queryWhereHasRecursive(subQuery, Object.entries(whereHas[1])));
            }
        }
        return builder;
    }
    queryOrderBy(builder, query) {
        if (query.orderBy) {
            let ordersBy = [];
            for (let index = 0; index < query.orderBy.length; index++) {
                const orderBy = query.orderBy[index].split(':');
                if (orderBy[0].includes('.')) {
                    const columns = orderBy[0].split('.');
                    const sortColumn = columns.pop();
                    const sortTable = this.queryOrderByRelationRecursive(builder, columns, builder.model);
                    if (sortTable) {
                        if (index == 0) {
                            builder.select(`${builder.model.table}.*`);
                        }
                        ordersBy.push({ column: `${sortTable}.${sortColumn}`, order: orderBy[1] ? orderBy[1] : 'desc' });
                    }
                }
                else {
                    ordersBy.push({ column: orderBy[0], order: orderBy[1] ? orderBy[1] : 'desc' });
                }
            }
            builder.orderBy(ordersBy);
        }
    }
    queryOrderByRelationRecursive(builder, relations, model) {
        const definition = relations.shift();
        if (!definition) {
            return '';
        }
        const relationDefinition = model.$relationsDefinitions.get(definition);
        const relatedModel = relationDefinition.relatedModel();
        if (!relationDefinition.options.localKey || !relationDefinition.options.foreignKey) {
            console.log("A relação precisa ser explícita");
            return '';
        }
        const localTable = model.table;
        const localKey = relationDefinition.options.localKey;
        const foreignTable = relatedModel.table;
        const foreignKey = relationDefinition.options.foreignKey;
        if (relationDefinition.type == 'hasOne') {
            builder.leftJoin(foreignTable, `${foreignTable}.${foreignKey}`, `${localTable}.${helpers_1.string.snakeCase(localKey)}`);
        }
        if (relationDefinition.type == 'belongsTo') {
            builder.leftJoin(foreignTable, `${foreignTable}.${localKey}`, `${localTable}.${helpers_1.string.snakeCase(foreignKey)}`);
        }
        if (relations.length > 0) {
            return this.queryOrderByRelationRecursive(builder, relations, relatedModel);
        }
        else {
            return foreignTable;
        }
    }
    querySearch(builder, query) {
        if (query.search) {
            builder.where(subBuilder => {
                this.querySearchRecursive(subBuilder, Object.entries(query.search));
            });
        }
    }
    querySearchRecursive(builder, searches) {
        for (let index = 0; index < searches.length; index++) {
            const search = searches[index];
            if (search[1].v) {
                let value = search[1].v;
                let operation = search[1].o;
                if (operation && operation.includes("LIKE")) {
                    value = operation.replace("LIKE", value).replace(/_/g, "%");
                    operation = "ILIKE";
                }
                const orFields = search[0].split(':');
                for (let index = 0; index < orFields.length; index++) {
                    builder.orWhere(orFields[index], operation || "=", value);
                }
            }
            else {
                builder.orWhereHas(search[0], (subQuery) => this.querySearchRecursive(subQuery, Object.entries(search[1])));
            }
        }
        return builder;
    }
    queryPreloads(builder, query) {
        if (query.preloads) {
            this.queryPreloadRecursive(builder, query.preloads.join(','));
        }
    }
    queryPreloadsWithWhereHas(builder, query) {
        if (query.preloadsWhereHas) {
            this.queryPreloadRecursive(builder, query.preloadsWhereHas.join(','), query.whereHas);
        }
    }
    queryPreloadRecursive(builder, preload, whereHas) {
        const preloads = preload.split(',');
        const trails = {};
        for (let index = 0; index < preloads.length; index++) {
            const items = preloads[index].split(':');
            if (items.length == 1) {
                const lastItem = items.shift();
                if (whereHas && lastItem && whereHas[lastItem]) {
                    builder.preload(lastItem, (subQuery) => {
                        this.queryWhere(subQuery, { where: whereHas[lastItem] });
                    });
                }
                else {
                    builder.preload(lastItem);
                }
            }
            else {
                const firstItem = items.shift();
                if (firstItem) {
                    trails[firstItem] = `${trails[firstItem] ? `${trails[firstItem]},` : ''}${items.join(':')}`;
                }
            }
        }
        const nextPreloads = Object.keys(trails);
        for (let index = 0; index < nextPreloads.length; index++) {
            builder.preload(nextPreloads[index], (subQuery) => {
                let subWhereHas;
                if (whereHas && whereHas[nextPreloads[index]]) {
                    subWhereHas = whereHas[nextPreloads[index]];
                    this.queryWhereHas(subQuery, { whereHas: subWhereHas });
                }
                this.queryPreloadRecursive(subQuery, trails[nextPreloads[index]], subWhereHas);
            });
        }
        return builder;
    }
    queryWithCounter(builder, query) {
        if (query.withCounts) {
            const withCounts = query.withCounts;
            for (let index = 0; index < withCounts.length; index++) {
                builder.withCount(withCounts[index]);
            }
        }
    }
}
exports.default = Query;
