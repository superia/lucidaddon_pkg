import SearchRequest from "./types/SearchRequest";
export default class Query<T> {
    model: any;
    database: any;
    constructor(database: any, model: T);
    create(record: Record<string, any>): Promise<any>;
    update(record: Record<string, any>): Promise<any>;
    delete(id: string): Promise<void>;
    find(query?: SearchRequest): Promise<{
        data: any;
    }>;
    findOrFail(id: string): Promise<{
        data: any;
    }>;
    findByOrFail(key: string, value: string): Promise<{
        data: any;
    }>;
    search(query?: SearchRequest): Promise<{
        meta?: any;
        data: any[];
    }>;
    private queryFields;
    private queryWhere;
    private queryWhereBetween;
    private queryWhereHas;
    private queryWhereHasRecursive;
    private queryOrderBy;
    private queryOrderByRelationRecursive;
    private querySearch;
    private querySearchRecursive;
    private queryPreloads;
    private queryPreloadsWithWhereHas;
    private queryPreloadRecursive;
    private queryWithCounter;
}
